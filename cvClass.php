<?php
class competence{
	private $secteur;
	private $specialite;
	private $nom;
	private $niveau;
	

	public function getSecteur(){
		return $this->secteur;
		}

	public function setSecteur($secteur){
		$this->secteur = $secteur;
		}

	public function getSpecialite(){
		return $this->specialite;
		}

	public function setSpecialite($specialite){
		$this->specialite = $specialite;
		}

	public function getNom(){
		return $this->nom;
		}

	public function setNom($nom){
		$this->nom = $nom;
		}

	public function getNiveau(){
		return $this->niveau;
		}

	public function setNiveau($niveau){
		$this->niveau = $niveau;
		}




	public function printCompetence($secteur, $specialite){
		if ($this->getSecteur() == $secteur && $this->getSpecialite() == $specialite) {
			return '<li>
					<div class="progress">
						<div class="progress-bar progress-bar-custom" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:'.$this->getNiveau().'%" title="'.$this->getNiveau().'%">
					    	<span class="">'.$this->getNom().'</span>
						</div>
					</div>
				</li>';
			}
		}
	}