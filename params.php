<?php

$Bdate = new DateTime('1981-09-24');
$Now = new DateTime();
$age = $Now->diff($Bdate);
$competences = array();

$competences['dev']['languages'][] = array("nom"=>"PHP","niveau"=>90);
$competences['dev']['languages'][] = array("nom"=>"MySql","niveau"=>80);
$competences['dev']['languages'][] = array("nom"=>"HTML5","niveau"=>80);
$competences['dev']['languages'][] = array("nom"=>"CSS3","niveau"=>80);
$competences['dev']['languages'][] = array("nom"=>"Javascript","niveau"=>90);
$competences['dev']['languages'][] = array("nom"=>"jQuery","niveau"=>65);
$competences['dev']['languages'][] = array("nom"=>"Twig","niveau"=>90);
$competences['dev']['languages'][] = array("nom"=>"Symfony","niveau"=>80);
$competences['dev']['languages'][] = array("nom"=>"Doctrine","niveau"=>80);



$competences['dev']['principes'][] = array("nom"=>"MVC","niveau"=>80);
$competences['dev']['principes'][] = array("nom"=>"POO","niveau"=>85);
$competences['dev']['principes'][] = array("nom"=>"Sass","niveau"=>50);
$competences['dev']['principes'][] = array("nom"=>"Ajax","niveau"=>80);
$competences['dev']['principes'][] = array("nom"=>"Tests Unit","niveau"=>40);
$competences['dev']['principes'][] = array("nom"=>"Sécurité","niveau"=>60);
$competences['dev']['principes'][] = array("nom"=>"Admin Sys","niveau"=>60);
$competences['dev']['principes'][] = array("nom"=>"Git","niveau"=>80);
$competences['dev']['principes'][] = array("nom"=>"Linux","niveau"=>70);

$competences['media']['2d'][] = array("nom"=>"Photographie","niveau"=>99);
$competences['media']['2d'][] = array("nom"=>"Adobe Photoshop","niveau"=>97);
$competences['media']['2d'][] = array("nom"=>"Adobe LightRoom","niveau"=>99);
$competences['media']['2d'][] = array("nom"=>"Adobe Illustrator","niveau"=>60);
$competences['media']['2d'][] = array("nom"=>"Adobe InDesign","niveau"=>55);

$competences['media']['3d'][] = array("nom"=>"Vidéo","niveau"=>70);
$competences['media']['3d'][] = array("nom"=>"Adobe Première","niveau"=>65);
$competences['media']['3d'][] = array("nom"=>"Adobe After Effect","niveau"=>60);
$competences['media']['3d'][] = array("nom"=>"SKetchUp","niveau"=>70);
$competences['media']['3d'][] = array("nom"=>"3DsMax","niveau"=>55);
$competences['media']['3d'][] = array("nom"=>"V-Ray","niveau"=>50);

$competences['divers']['bureautique'][] = array("nom"=>"Excel","niveau"=>80);
$competences['divers']['bureautique'][] = array("nom"=>"Word","niveau"=>70);

$competences['divers']['langues'][] = array("nom"=>"Anglais lu","niveau"=>90);
$competences['divers']['langues'][] = array("nom"=>"Anglais parlé","niveau"=>70);
$competences['divers']['langues'][] = array("nom"=>"Anglais ecrit","niveau"=>60);
$competences['divers']['langues'][] = array("nom"=>"Espagnol parlé","niveau"=>30);
