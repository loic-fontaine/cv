<?php
require_once("params.php");
require_once("cvClass.php");

$progressBars = array();
foreach ($competences as $secteur => $specialites) {
	foreach ($specialites as $specialite => $pbs) {
		foreach ($pbs as $pb) {
			$c = new competence;
			$c->setSecteur($secteur);
			$c->setSpecialite($specialite);
			$c->setNom($pb['nom']);
			$c->setNiveau($pb['niveau']);
			$progressBars[] = $c;
			}
		}
	}

require_once("index_view.html");